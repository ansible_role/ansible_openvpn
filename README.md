# OpenVPN ansible role

This role is intented for installing and configuring openvpn server on linux servers under Debian-like and Redhat-like operating systems.

For certificate management it uses EasyRSA scripts.

For certificate fetching from remote server, you can use *openvpn_fetch_** params.

### Playbook example
```yaml
- hosts: vpn
  roles:
    - '.'
  vars:
    openvpn_name: vpn
    openvpn_remotes: 
        - example.com
    openvpn_dev: tun
    openvpn_port: 1194
    openvpn_server: 172.30.3.0 255.255.255.0
    openvpn_pool: 172.30.3.100 172.30.3.110
    openvpn_topology: subnet
    openvpn_max_clients: 2
    openvpn_client_to_client: no
    openvpn_comp_lzo: yes
    openvpn_tls_auth: yes
    openvpn_server_options:
      - push "route 172.30.3.0 255.255.255.0"
      - push "route 192.168.1.0 255.255.255.0"

    openvpn_key_country: USA
    openvpn_key_province: NY
    openvpn_key_city: NYC
    openvpn_key_org: IT
    openvpn_key_email: admin@example.com

    openvpn_clients:
      - client1
      - client2

    openvpn_ccd_options:
      client1:
        - iroute 192.168.1.0 255.255.255.0
```

